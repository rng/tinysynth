tinysynth: tinysynth.c tinysynth.h
	gcc -o $@ -Wall -O2 -std=c99 `sdl-config --cflags` $< `sdl-config --libs` -lm

clean:
	rm -rf tinysynth
