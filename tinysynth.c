#include <SDL/SDL.h>
#include <stdint.h>
#include <assert.h>

#define SYNTH_WAIT_MS(delay) SDL_Delay(delay)
#define SYNTH_LOCK_MIXER() SDL_LockAudio()
#define SYNTH_UNLOCK_MIXER() SDL_UnlockAudio()

#include "tinysynth.h"

void audio_callback(void *userdata, uint8_t *stream, int len)
{
    int16_t buf[512];
    synth_mix(buf, len/2);
    SDL_memcpy(stream, buf, len);
}

int main(int argc, char* argv[])
{
    SDL_AudioSpec desiredSpec, obtainedSpec;

    if (SDL_Init(SDL_INIT_AUDIO) < 0) {
        exit(-1);
    }

    desiredSpec.freq = SAMPLE_RATE;
    desiredSpec.format = AUDIO_S16SYS;
    desiredSpec.channels = 1;
    desiredSpec.samples = 1024;
    desiredSpec.callback = audio_callback;
    desiredSpec.userdata = NULL;

    if (SDL_OpenAudio(&desiredSpec, &obtainedSpec) < 0){
        exit(-1);
    }

    FILE *f = fopen(argv[1], "rb");
    uint32_t framecount = 0;
    size_t nread = fread(&framecount, 4, 1, f);
    assert(nread == 1);
    uint32_t *song = (uint32_t*)malloc(framecount*4);
    nread = fread(song, 4, framecount, f);
    assert(nread == framecount);
    fclose(f);

    SDL_PauseAudio(0);
    synth_play(song, framecount);
    SDL_CloseAudio();
}
