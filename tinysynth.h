#include <stdint.h>
#include <math.h>

#define NCH (4)
#define SAMPLE_RATE (22050)
#define TWOPI (2*3.14159)

typedef enum {
    WF_SIN,
    WF_SQR,
    WF_NOISE,
} waveform_t;

typedef struct {
    uint8_t note;
    uint16_t pos;
} channel_t;

typedef struct {
    uint16_t attack;
    uint16_t decay;
    uint8_t vol;
    waveform_t waveform;
} instrument_t;

channel_t channels[NCH];

instrument_t instruments[NCH] = {
    {1000, 2800, 67, WF_SQR},
    {1000, 2500, 67, WF_SQR},
    {1000, 4000, 73, WF_SIN},
    { 500, 1500, 23, WF_NOISE},
};

float notetable[] = {
    27.5, 29.1, 30.9, 32.7, 34.6, 36.7, 38.9, 41.2, 43.7, 46.2, 49.0, 51.9, 55.0,
    58.3, 61.7, 65.4, 69.3, 73.4, 77.8, 82.4, 87.3, 92.5, 98.0, 103.8, 110.0, 116.5,
    123.5, 130.8, 138.6, 146.8, 155.6, 164.8, 174.6, 185.0, 196.0, 207.7, 220.0,
    233.1, 246.9, 261.6, 277.2, 293.7, 311.1, 329.6, 349.2, 370.0, 392.0, 415.3,
    440.0, 466.2, 493.9, 523.3, 554.4, 587.3, 622.3, 659.3, 698.5, 740.0, 784.0,
    830.6, 880.0, 932.3, 987.8, 1046.5, 1108.7, 1174.7, 1244.5, 1318.5, 1396.9,
    1480.0, 1568.0, 1661.2, 1760.0, 1864.7
};

float channel_envelope(instrument_t *ins, uint16_t pos)
{
    if (pos <= ins->attack) {
        return pos/(float)ins->attack;
    } else if (pos <= (ins->attack + ins->decay)) {
        return 1. - (pos - ins->attack)/(float)ins->decay;
    }
    return 0.0;
}

float channel_tick(channel_t *ch, instrument_t *ins)
{
    if (!ch->note) return 0;
    float env = channel_envelope(ins, ch->pos) * ((float)ins->vol)/255.;
    float freq = notetable[ch->note + 3];
    int32_t period = SAMPLE_RATE / freq;
    float v = 0;
    switch (ins->waveform) {
        case WF_SIN:
            v = 0.5 * sinf(TWOPI*freq*ch->pos/(float)SAMPLE_RATE);
            break;
        case WF_SQR:
            v = ((ch->pos % period) < (period/2)) ? 0.5 : -0.5;
            break;
        case WF_NOISE:
            v = (float)rand()/(float)(RAND_MAX) - 0.5;
            break;
    }
    ch->pos++;
    return v * env;
}

void channel_play(channel_t *ch, uint8_t note) {
    if (note) {
        ch->pos = 0;
        ch->note = note;
    }
}

void synth_play(uint32_t *song, size_t song_len) {
    for (int pos=0; pos < song_len; pos++) {
        uint32_t frame = song[pos];
        float delay = 1000 * (frame>>24) / 128.;
        uint8_t n0 = (frame    ) & 63;
        uint8_t n1 = (frame>>6 ) & 63;
        uint8_t n2 = (frame>>12) & 63;
        uint8_t n3 = (frame>>18) & 63;

        SYNTH_WAIT_MS(delay);
        SYNTH_LOCK_MIXER();
        channel_play(&channels[0], n0);
        channel_play(&channels[1], n1);
        channel_play(&channels[2], n2);
        channel_play(&channels[3], n3);
        SYNTH_UNLOCK_MIXER();
    }
}

void synth_mix(int16_t *buf, size_t nsamples) {
    for (int i=0; i<nsamples; i++) {
        float sample = 0.;
        for (int ch=0; ch<NCH; ch++) {
            sample += channel_tick(&channels[ch], &instruments[ch]);
        }
        buf[i] = (int16_t)(sample*32767.);
    }
}
